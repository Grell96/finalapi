import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:pelicula/pelis.dart';

class MovieDetail extends StatelessWidget {
  final id;

  MovieDetail(this.id);
  final TextEditingController _titleController = new TextEditingController();
  final TextEditingController _quaController = new TextEditingController();
  final TextEditingController _priceController = new TextEditingController();
  _onPressed() {}

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      home: Scaffold(
        body: Container(
          child: ListView(
            padding: const EdgeInsets.only(
                top: 62, left: 12.0, right: 12.0, bottom: 12.0),
            children: <Widget>[
              Container(
                height: 50,
                child: new TextField(
                  controller: _titleController
                    ..text = (id == -1) ? "" : movies[id]['title'].toString(),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    labelText: 'Titulo',
                    hintText: 'Ingrese el titulo',
                    icon: new Icon(Icons.email),
                  ),
                ),
              ),
              Container(
                height: 50,
                child: new TextField(
                  controller: _quaController
                    ..text =
                        (id == -1) ? "" : movies[id]['quantity'].toString(),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Cantidad',
                    hintText: 'Ingrese la cantidad',
                    icon: new Icon(Icons.vpn_key),
                  ),
                ),
              ),
              Container(
                height: 50,
                child: new TextField(
                  controller: _priceController
                    ..text = (id == -1) ? "" : movies[id]['price'].toString(),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Precio',
                    hintText: 'Ingrese el precio',
                    icon: new Icon(Icons.vpn_key),
                  ),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 44.0),
              ),
              Container(
                height: 50,
                child: new RaisedButton(
                  onPressed: () {
                    var temp = {
                      'title': _titleController.text,
                      'quantity': _quaController.text,
                      'price': _priceController.text,
                    };
                    Navigator.pop(context, temp);
                  },
                  color: Colors.blue,
                  child: new Text(
                    'Guardar',
                    style: new TextStyle(
                        color: Colors.white, backgroundColor: Colors.blue),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
