import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:pelicula/pelis.dart';
import 'movie_detail.dart';

class MovieList extends StatefulWidget {
  @override
  MovieListState createState() {
    return new MovieListState();
  }
}

class MovieListState extends State<MovieList> {
  Color mainColor = const Color(0xff3C3261);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        elevation: 0.3,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
          'Movies',
          style: new TextStyle(
            color: mainColor,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: new Padding(
        padding: const EdgeInsets.all(16.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Expanded(
              child: new ListView.builder(
                  itemCount: movies == null ? 0 : movies.length,
                  itemBuilder: (context, i) {
                    return new Container(
                      child: new MovieCell(movies, i),
                      padding: const EdgeInsets.all(0.0),
                      color: Colors.white,
                    );
                  }),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final result = await Navigator.push(context,
              new MaterialPageRoute(builder: (context) {
            return new MovieDetail(-1);
          }));
          setState(() {
            movies.add(result);
          });
        },
        child: const Icon(Icons.add),
        backgroundColor: Colors.blue,
      ),
    );
  }
}

class MovieTitle extends StatelessWidget {
  final Color mainColor;

  MovieTitle(this.mainColor);

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      child: new Text(
        'Top Rated',
        style: new TextStyle(
            fontSize: 40.0,
            color: mainColor,
            fontWeight: FontWeight.bold,
            fontFamily: 'Arvo'),
        textAlign: TextAlign.left,
      ),
    );
  }
}

class MovieCell extends StatefulWidget {
  final movies;
  final i;
  MovieCell(this.movies, this.i);

  @override
  MovieCell1 createState() => MovieCell1();
}

class MovieCell1 extends State<MovieCell> {
  Color mainColor = const Color(0xff3C3261);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        (widget.i < movies.length)
            ? new Row(
                children: <Widget>[
                  new Expanded(
                      child: new Container(
                    margin: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
                    child: new Column(
                      children: [
                        new Text(
                          widget.movies[widget.i]['title'],
                          style: new TextStyle(
                              fontSize: 20.0,
                              fontFamily: 'Arvo',
                              fontWeight: FontWeight.bold,
                              color: mainColor),
                        ),
                        new Padding(padding: const EdgeInsets.all(2.0)),
                        new Text(
                          "Cantidad: ${widget.movies[widget.i]['quantity'].toString()}",
                          maxLines: 1,
                          style: new TextStyle(
                              color: const Color(0xff8785A4),
                              fontFamily: 'Arvo'),
                        ),
                        new Text(
                          "Precio: ${widget.movies[widget.i]['price'].toString()}",
                          maxLines: 1,
                          style: new TextStyle(
                              color: const Color(0xff8785A4),
                              fontFamily: 'Arvo'),
                        )
                      ],
                      crossAxisAlignment: CrossAxisAlignment.start,
                    ),
                  )),
                  IconButton(
                      onPressed: () async {
                        final result = await Navigator.push(context,
                            new MaterialPageRoute(builder: (context) {
                          return new MovieDetail(widget.i);
                        }));
                        setState(() {
                          movies[widget.i] = result;
                        });
                      },
                      icon: Icon(
                        Icons.edit,
                        color: Colors.black,
                      )),
                  IconButton(
                      onPressed: () {
                        setState(() {
                          movies.removeAt(widget.i);
                        });
                      },
                      icon: Icon(
                        Icons.delete,
                        color: Colors.black,
                      ))
                ],
              )
            : Container(),
        new Container(
          width: 300.0,
          height: 0.5,
          color: const Color(0xD2D2E1ff),
          margin: const EdgeInsets.all(16.0),
        )
      ],
    );
  }
}
