var movies = [
  {
    'title': 'Pelicula 1',
    'quantity': 20,
    'price': 13.5,
  },
  {
    'title': 'Pelicula 2',
    'quantity': 40,
    'price': 3.5,
  },
  {
    'title': 'Pelicula 3',
    'quantity': 25,
    'price': 1.5,
  },
  {
    'title': 'Pelicula 4',
    'quantity': 15,
    'price': 14.5,
  },
];
